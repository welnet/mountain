import { ROUTES } from './../../routes.const';
import { Component } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  ROUTES = ROUTES;
  logoUrl = '/assets/images/logo.png';

}
