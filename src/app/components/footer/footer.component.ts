import { Component } from '@angular/core';
import { ROUTES } from './../../routes.const';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {

  ROUTES = ROUTES;

}
