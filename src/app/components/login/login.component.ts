import { AuthService } from './../../services/auth.service';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ROUTES } from '../../routes.const';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  ROUTES = ROUTES;

  constructor(
    private auth: AuthService,
    private router: Router
  ) { }

  loginUser(event) {
    event.preventDefault();
    const target = event.target;
    const login = target.querySelector('#login').value;
    const password = target.querySelector('#password').value;

    this.auth.getUserDetails(login, password).subscribe(data => {
      if (data.status === 'ok') {
        this.router.navigate([ROUTES.ADMIN]);
        this.auth.setLoggedIn(true);
      } else {
        window.alert(data.message);
      }
    });
    console.log(login, password);
  }

}
