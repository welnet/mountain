export enum ROUTES {
    HOME = '/',
    ABOUT = '/about',
    OFFER = '/offer',
    GALLERY = '/gallery',
    CONTACT = '/contact',
    COOKIES = '/cookies',
    PRIVACY = '/privacy',
    ADMIN = '/admin'
}
