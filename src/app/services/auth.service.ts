import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ROUTES } from '../routes.const';

interface MyData {
  message: string;
  status: string;
}

const TOKEN = 'TOKEN';

@Injectable()
export class AuthService {

  ROUTES = ROUTES;

  private loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false');

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  setLoggedIn(value: boolean) {
    this.loggedInStatus = value;
    localStorage.setItem('loggedIn', 'true');
  }

  get isLoggedIn() {
    return JSON.parse(localStorage.getItem('loggedIn') || this.loggedInStatus.toString());
  }

  getUserDetails(login, password) {
    return this.http.post<MyData>('/login', {
      login,
      password
    });
  }

  logout(): void {
    this.router.navigate([ROUTES.HOME]);
    localStorage.clear();
  }
}
